import { graphql, useStaticQuery } from "gatsby";

export const useSiteMetadata = () => {
  const { site } = useStaticQuery(
    graphql`
      query SITE_METADATA_QUERY {
        site {
          siteMetadata {
            title
            main_page
            second_page
            third_page
            third_page_link
          }
        }
      }
    `
  );
  return site.siteMetadata;
};

import React from "react";
import styled from "styled-components";
import { TopNavbar } from "./TopNavbar";

const AppStyles = styled.main`
  padding: 2em;
  margin: auto;
`;

export const Layout = ({ children }) => {
  return (
    <AppStyles>
      <TopNavbar />
      {children}
    </AppStyles>
  );
};

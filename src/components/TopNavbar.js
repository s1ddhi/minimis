import React from "react";
import { useSiteMetadata } from "../hooks/useSiteMetadata";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

export const TopNavbar = () => {
  const {
    title,
    main_page,
    second_page,
    third_page,
    third_page_link,
  } = useSiteMetadata();
  return (
    <Navbar expand="lg">
      <Navbar.Brand href="/">{title}</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <Nav.Item>
            <Nav.Link href="/">{main_page}</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link href={`/${second_page}`}>{second_page}</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link href={third_page_link}>{third_page}</Nav.Link>
          </Nav.Item>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

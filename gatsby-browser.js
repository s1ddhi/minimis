import { wrapRootElement as wrap } from "./root-wrapper";
import "bootstrap/dist/css/bootstrap.css";

export const wrapRootElement = wrap;

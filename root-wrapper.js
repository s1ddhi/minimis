import { MDXProvider } from "@mdx-js/react";
import React from "react";
import Code from "./src/components/Code";

const components = {
  pre: ({ children: { props } }) => {
    if (props.mdxType === "code") {
      const language =
        props.className && props.className.replace("language-", "");
      return (
        <Code
          codeString={props.children.trim()}
          language={language}
          {...props}
        />
      );
    }
  },
};

export const wrapRootElement = ({ element }) => (
  <MDXProvider components={components}>{element}</MDXProvider>
);
